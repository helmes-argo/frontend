import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: null,
    token: null,
    users: [],
  },
  mutations: {
    initialiseStore(state) {
			if(localStorage.getItem('store')) {
				(<any>this).replaceState(Object.assign(state, JSON.parse(<string>localStorage.getItem('store'))));
			}
		},
    updateUser: (state, user) => {
      state.user = user;
    },
    updateToken: (state, token) => {
      state.token = token;
    },
    updateUsers: (state, users) => {
      state.users = users;
    },
    addUser: (state, user) => {
      (<any[]>state.users).push(user);
    },
  },
  getters: {
    getUser: state => state.user,
    getToken: state => state.token,
    getUsers: state => state.users,
  },
  actions: {}
});
