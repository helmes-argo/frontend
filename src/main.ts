import Vue from "vue";
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import Multiselect from 'vue-multiselect'

import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.use(BootstrapVue);

Vue.config.productionTip = false;

// register globally
Vue.component('multiselect', Multiselect)

store.subscribe((mutation, state) => {
	localStorage.setItem('store', JSON.stringify(state));
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
